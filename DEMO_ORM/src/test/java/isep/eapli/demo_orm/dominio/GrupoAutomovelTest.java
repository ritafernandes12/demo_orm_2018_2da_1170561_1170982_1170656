/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import static isep.eapli.demo_orm.dominio.Classe.UTILITARIO;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rita
 */
public class GrupoAutomovelTest {

    public GrupoAutomovelTest() {
    }

    /**
     * Test of alterarClasse method, of class GrupoAutomovel.
     */
    @Test
    public void testAlterarClasse() {
        Classe novaClasse = null;
        GrupoAutomovel instance = new GrupoAutomovel("Teste", 2, 21.2f, UTILITARIO);
        try {
            instance.alterarClasse(novaClasse);
            fail();
        } catch (IllegalArgumentException expected) {
            assertTrue(true);
        }
    }

    /**
     * Test of alterarNumeroPortas method, of class GrupoAutomovel.
     */
    @Test
    public void testAlterarNumeroPortas() {
        GrupoAutomovel instance = new GrupoAutomovel("Teste", 2, 21.2f, UTILITARIO);
        try {
            instance.alterarNumeroPortas(0);
            fail();
        } catch (IllegalArgumentException expected) {
            assertTrue(true);
        }
    }

    /**
     * Test of toString method, of class GrupoAutomovel.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        GrupoAutomovel instance = new GrupoAutomovel("Teste", 2, 21.2f, UTILITARIO);
        String expResult = "Grupo Automóvel Teste com preço " + 21.2f + ", nº de portas:2 e classe" + UTILITARIO.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

}
