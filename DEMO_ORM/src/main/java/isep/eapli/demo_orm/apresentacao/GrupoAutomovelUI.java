/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.apresentacao;

import isep.eapli.demo_orm.aplicacao.GrupoAutomovelController;
import isep.eapli.demo_orm.dominio.Classe;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import isep.eapli.demo_orm.util.Console;
import java.util.List;

/**
 *
 * @author mcn
 */
public class GrupoAutomovelUI {

    private final GrupoAutomovelController controller = new GrupoAutomovelController();

    public void registarGA() {
        System.out.println("*** Registo Grupo Automóvel ***\n");
        String nome = Console.readLine("Nome:");
        int portas = Console.readInteger("Número de portas");
        System.out.println("Escolha uma das classes:\n");
        int classe = Console.readInteger("1 - Utilitário;\n2- Luxo;\n3 - Comercial.\n");
        Classe cla = null;
        try{
            switch (classe) {
                case 1:
                    cla = Classe.UTILITARIO;
                    break;
                case 2:
                    cla = Classe.LUXO;
                    break;
                case 3:
                    cla = Classe.COMERCIAL;
                default:
                    break;
            }
        }catch(NumberFormatException e){
            System.out.println("Opção inválida.");
        }
        float preco = (float) Console.readDouble("Preço: ");
        if(cla == null)
            System.out.println("O registo não foi efetuado. Try again!");
        else{
            GrupoAutomovel grupoAutomovel = controller.
                registarGrupoAutomóvel(nome, portas, preco, cla);
            System.out.println("Grupo Automóvel" + grupoAutomovel);
        }
    }

    public void listarGAs() {
        System.out.println("Listagem dos Grupos Automóveis:\n");
        controller.listarGruposAutomoveis().forEach((ga) -> {
            System.out.println(ga + "\n");
        });
        System.out.println("\n");
    }

    public void procurarGAPorID(long id) {
        System.out.println(controller.procurarGrupoAutomovel(id) + "\n");
    }
}
