/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

/**
 *
 * @author Ana Isabel Fonseca
 */
public class Automovel {
    
    private String matricula;
    private double km;
    private int anoFabrico;
    private int anoAquisicao;
    private String empresa;
    private String cor;
    private int cilindrada;
    private GrupoAutomovel grupoAutomovel;
    
    
    /**
     * Automovel constructor.
     * @param matricula
     * @param km
     * @param anoFabrico
     * @param anoAquisicao
     * @param empresa
     * @param cor
     * @param cilindrada
     * @param grupoAutomovel 
     */
    public Automovel(String matricula, double km, int anoFabrico, int anoAquisicao, String empresa, String cor, int cilindrada, GrupoAutomovel grupoAutomovel) {
        if(matricula == null || matricula.split("-").length != 3){
            throw new IllegalArgumentException();
        }
    
        if(km < 0 || cor == null || grupoAutomovel == null || empresa == null || cilindrada < 0 || anoAquisicao < 0 || anoAquisicao < 1769 || anoFabrico < 1769 || anoFabrico < 0){
            throw new IllegalArgumentException();
        }
        
        this.matricula = matricula;
        this.km = km;
        this.anoFabrico = anoFabrico;
        this.anoAquisicao = anoAquisicao;        
        this.empresa = empresa;       
        this.cor = cor;
        this.cilindrada = cilindrada;
        this.grupoAutomovel = grupoAutomovel;
    }
    
        
    /**
     * Método que altera o número de km do Automóvel
     * @param km: novos km.
     */
    public void alterarNumeroKm(double km){
        if(km < 0){
            throw new IllegalArgumentException();
        }
        this.km = km;
    }
    
    /**
     * Método que retorna o valor da matrícula
     * @return Matrícula do Automóvel.
     */
    public String matricula(){
        return matricula;
    }
    
}
