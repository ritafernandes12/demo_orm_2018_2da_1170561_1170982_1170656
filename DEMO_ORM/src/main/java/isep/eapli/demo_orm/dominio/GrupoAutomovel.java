/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Rita
 */
@Entity
public class GrupoAutomovel implements Serializable {

   

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nomeGrupo;
    private int numPortas;
    private float precoPorDia;
    @Enumerated(EnumType.STRING)
    private Classe classe;

     protected GrupoAutomovel() {
    }
     
    /**
     * Construtor público
     *
     * @param nomeGrupo nome do grupo
     * @param numPortas número de portas
     * @param precoPorDia preços por dia
     * @param classe Classe do automóvel
     */
    public GrupoAutomovel(String nomeGrupo, int numPortas, float precoPorDia, Classe classe) {
        if (numPortas < 0 || precoPorDia < 0.0 || classe == null || nomeGrupo == null) {
            throw new IllegalArgumentException();
        }
        this.nomeGrupo = nomeGrupo;
        this.numPortas = numPortas;
        this.precoPorDia = precoPorDia;
        this.classe = classe;
    }

    /**
     * Método que permite alterar a classe do grupo
     *
     * @param novaClasse nova classe do grupo
     */
    public void alterarClasse(Classe novaClasse) {
        if (novaClasse == null) {
            throw new IllegalArgumentException();
        }
        classe = novaClasse;
    }

    /**
     * Método para alterar o número de portas.
     *
     * @param numPortas: novo número de portas.
     */
    public void alterarNumeroPortas(int numPortas) {
        if (numPortas < 1) {
            throw new IllegalArgumentException();
        }
        this.numPortas = numPortas;
    }

    /**
     * Método toString da classe grupoAutomovel
     */
    @Override
    public String toString() {
        return "Grupo Automóvel " + nomeGrupo + " com preço " + precoPorDia + ", nº de portas:"
                + numPortas + " e classe" + classe;
    }
}
